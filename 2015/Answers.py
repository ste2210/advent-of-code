import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import hashlib as hsh
import re
import itertools

# %% TASK 1 & 2
data = open(r"C:\Users\ste22\OneDrive\Documents\Python Scripts\Advent of Code\2015\Task 1.txt").read()

count = 0
neg_pos = None
for pos, char in enumerate(data):
    if char == '(':
        count += 1
    elif char == ")":
        count -= 1
        if (count < 0) & (neg_pos is None):
            neg_pos = pos + 1
            
print(f'Answer 1: {count}')
print(f'Answer 2: {neg_pos}')

# %% TASK 3 & 4
data = pd.read_csv(r"C:\Users\ste22\OneDrive\Documents\Python Scripts\Advent of Code\2015\Task 3.txt",
                   delimiter="x",
                   names=['l', 'w', 'h'])

data['side_1'] = (2 * data.l * data.w)
data['side_2'] = (2 * data.w * data.h)
data['side_3'] = (2 * data.l * data.h)
data['small_side'] = data[['side_1', 'side_2', 'side_3']].min(axis = 1) / 2
data['surface_area'] = data.side_1 + data.side_2 + data.side_3 + data.small_side
total_paper = data.surface_area.sum()

data['perim_1'] = (2 * data.l) + (2 * data.w)
data['perim_2'] = (2 * data.w) + (2 * data.h)
data['perim_3'] = (2 * data.l) + (2 * data.h)
data['small_perim'] = data[['perim_1', 'perim_2', 'perim_3']].min(axis = 1)
data['volume'] = data.l * data.w * data.h
total_ribbon = (data.small_perim + data.volume).sum()

print(f'Answer 3: {total_paper}')
print(f'Answer 4: {total_ribbon}')

# %% TASK 5 & 6
data = open(r"C:\Users\ste22\OneDrive\Documents\Python Scripts\Advent of Code\2015\Task 5.txt").read()

santa_map_1 = np.zeros((200,200))
x = 100
y = 100
santa_map_1[x][y] = 1

for direction in data:
    if direction == '^':
        y -= 1
    elif direction == '>':
        x += 1
    elif direction == 'v':
        y += 1
    elif direction == '<':
        x -= 1
    
    santa_map_1[x][y] += 1

santa_map_2 = np.zeros((200,200))
s_x = 100
s_y = 100
r_x = 100
r_y = 100
santa_map_2[s_x][s_y] = 2

for i, direction in enumerate(data):
    if i % 2 == 0:
        if direction == '^':
            s_y -= 1
        elif direction == '>':
            s_x += 1
        elif direction == 'v':
            s_y += 1
        elif direction == '<':
            s_x -= 1
        santa_map_2[s_x][s_y] += 1
        
    if i % 2 == 1:
        if direction == '^':
            r_y -= 1
        elif direction == '>':
            r_x += 1
        elif direction == 'v':
            r_y += 1
        elif direction == '<':
            r_x -= 1
        santa_map_2[r_x][r_y] += 1
    

present_received_1 = (santa_map_1 > 0).sum()
present_received_2 = (santa_map_2 > 0).sum()

print(f'Answer 5: {present_received_1}')
print(f'Answer 6: {present_received_2}')

# Just for fun    
plt.imshow(santa_map_1, cmap='hot', interpolation='nearest')
plt.imshow(santa_map_2, cmap='hot', interpolation='nearest')

# %% TASK 7 & 8

puzzle_input = 'ckczppom'

i=0
while True:
    result = hsh.md5((puzzle_input+str(i)).encode())
    if result.hexdigest()[:5] == "00000":
        print(f'Answer 7: {i}')
        break
    i+=1

i=0
while True:
    result = hsh.md5((puzzle_input+str(i)).encode())
    if result.hexdigest()[:6] == "000000":
        print(f'Answer 8: {i}')
        break
    i+=1


# %% TASK 9 & 10
data = pd.read_csv(r"C:\Users\ste22\OneDrive\Documents\Python Scripts\Advent of Code\2015\Task 9.txt",
                   names = ['string'])

def vowel_count(string):
    count = sum(map(string.count, ('a', 'e', 'i', 'o', 'u')))
    return count

def double_letter(string):
    for i in range(0, len(string)-1):
        if string[i] == string[i+1]:
            double = 'Y'
            break
        else:
            double = 'N'
    return double

def bad_str_count(string):
    count = sum(map(string.count, ('ab', 'cd', 'pq', 'xy',)))
    return count


data['vowel_count'] = np.vectorize(vowel_count)(data.string)
data['double_letter'] = np.vectorize(double_letter)(data.string)
data['bad_str_count'] = np.vectorize(bad_str_count)(data.string)

nice = (data.vowel_count >= 3) & (data.double_letter == 'Y') & (data.bad_str_count == 0)
nice_strings = data[nice].shape[0]

print(f'Answer 9: {nice_strings}')


def repeat_pair(string):
    count = 0
    for i in range(0, len(string)-1):
        substr = string[i]+string[i+1]
        count += (string.count(substr) -1)
    return count

def repeat_sep_one(string):
    for i in range(0, len(string)-2):
        if (string[i] == string[i+2]) & (string[i] != string[i+1]):
            double = 'Y'
            break
        else:
            double = 'N'
    return double
    

data['repeat_pair_count'] = np.vectorize(repeat_pair)(data.string)
data['rep_sep_one_count'] = np.vectorize(repeat_sep_one)(data.string)


nice_2 = (data.repeat_pair_count >= 1) & (data.rep_sep_one_count == 'Y')
nice_strings_2 = data[nice_2].shape[0]
print(f'Answer 10: {nice_strings_2}')

# %% TASK 11 & 12


def turn_on(array, start, finish):
    s_x, s_y = start.split(",")
    f_x, f_y = finish.split(",")
    array[int(s_x):int(f_x)+1, int(s_y):int(f_y)+1] = 1
    return array

def turn_off(array, start, finish):
    s_x, s_y = start.split(",")
    f_x, f_y = finish.split(",")
    array[int(s_x):int(f_x)+1, int(s_y):int(f_y)+1] = 0
    return array

def toggle(array, start, finish):
    s_x, s_y = start.split(",")
    f_x, f_y = finish.split(",")
    window = array[int(s_x):int(f_x)+1, int(s_y):int(f_y)+1]
    window[window == 1] = 2
    window[window == 0] = 1
    window[window == 2] = 0
    return array

light_array = np.zeros((1000,1000))

for line in open(r"C:\Users\ste22\OneDrive\Documents\Python Scripts\Advent of Code\2015\Task 11.txt").readlines():
    instruction = line.replace("\n", "").split()
    if instruction[0] == 'toggle':
        toggle(light_array, instruction[1], instruction[3])
    else:
        if instruction[1] == 'on':
            turn_on(light_array, instruction[2], instruction[4])
        elif instruction[1] == 'off':
            turn_off(light_array, instruction[2], instruction[4])
            
print(f'Answer 11: {light_array.sum()}')


def turn_on(array, start, finish):
    s_x, s_y = start.split(",")
    f_x, f_y = finish.split(",")
    array[int(s_x):int(f_x)+1, int(s_y):int(f_y)+1] += 1
    return array

def turn_off(array, start, finish):
    s_x, s_y = start.split(",")
    f_x, f_y = finish.split(",")
    window = array[int(s_x):int(f_x)+1, int(s_y):int(f_y)+1]
    window -= 1
    window[window < 0] = 0
    return array

def toggle(array, start, finish):
    s_x, s_y = start.split(",")
    f_x, f_y = finish.split(",")
    array[int(s_x):int(f_x)+1, int(s_y):int(f_y)+1] += 2
    return array

light_array = np.zeros((1000,1000))

for line in open(r"C:\Users\ste22\OneDrive\Documents\Python Scripts\Advent of Code\2015\Task 11.txt").readlines():
    instruction = line.replace("\n", "").split()
    if instruction[0] == 'toggle':
        toggle(light_array, instruction[1], instruction[3])
    else:
        if instruction[1] == 'on':
            turn_on(light_array, instruction[2], instruction[4])
        elif instruction[1] == 'off':
            turn_off(light_array, instruction[2], instruction[4])
            
print(f'Answer 12: {light_array.sum()}')
    
# %% TASK 13 & 14

file = r"C:\Users\ste22\OneDrive\Documents\Python Scripts\Advent of Code\2015\Task 13.txt"

wires = {wire.split()[-1]:wire.split("->")[0].rstrip().split() for wire in open(file).readlines()}

def calc_bitwise(operation, inp_a, inp_b=0xffff):
    match operation:
        case "LSHIFT":
            return  int(inp_a) << int(inp_b)
        case "RSHIFT":
            return  int(inp_a) >> int(inp_b)
        case "AND":
            return  int(inp_a) & int(inp_b)
        case "OR":
            return  int(inp_a) | int(inp_b)
        case _:
            return ~int(inp_a) & int(inp_b)
 
def get_input(result):
    inp = wires[result]
    if type(inp) == int:
        return inp
    elif len(inp) == 1:
        if inp[0].isdigit():
            wires[result] = int(inp[0])
            return wires[result]
        else:
            wires[result] = get_input(inp[0])
            return wires[result]
    elif len(inp) == 2:
        if inp[1].isdigit():
            wires[result] = calc_bitwise(inp[0], inp[1])
            return wires[result]
        else:
            wires[result] = calc_bitwise(inp[0], get_input(inp[1]))
            return wires[result]
    elif len(inp) == 3:
        if (inp[0].isdigit()) & (inp[2].isdigit()):
            wires[result] = calc_bitwise(inp[1], inp[0], inp[2])
            return wires[result]
        elif (inp[0].isdigit()):
            wires[result] = calc_bitwise(inp[1], inp[0], get_input(inp[2]))
            return wires[result]
        elif (inp[2].isdigit()):
            wires[result] = calc_bitwise(inp[1], get_input(inp[0]), inp[2])
            return wires[result]
        else:
            wires[result] = calc_bitwise(inp[1],get_input(inp[0]), get_input(inp[2]))
            return wires[result]

result = get_input('a')

print(f"Answer 13: {result}")

wires = {wire.split()[-1]:wire.split("->")[0].rstrip().split() for wire in open(file).readlines()}

wires['b'] = result

result_2 = get_input('a')

print(f"Answer 14: {result_2}")

# %% TASK 15 & 16

def find_char_diff(line):
    raw_string = line.rstrip("\n") 
    string = raw_string.rstrip("\"").lstrip("\"")
    num_chars = len(raw_string)
    
    esc_quotes = len(re.findall(r"\\\"", string))
    esc_hex = len(re.findall(r"\\x\w\w", string))
    esc_slash = len(re.findall(r"\\\\", string))
    
#    if (esc_quotes > 0) or (esc_slash > 0) or (esc_hex > 0):
 #       print(f'{string} contains {esc_quotes} quotes, {esc_slash} slashes and {esc_hex} hex')
    
    num_bytes = len(string) - (esc_quotes + esc_slash + (3 * esc_hex))
    return num_chars - num_bytes


file = r"C:\Users\ste22\OneDrive\Documents\Python Scripts\Advent of Code\2015\Task 15.txt"
tot = 0

for line in open(file).readlines():
    tot += find_char_diff(line)
    
#print(tot)

# %% TASK 17 & 18

dist_array = {}
towns = []

for line in open(r"C:\Users\ste22\OneDrive\Documents\Python Scripts\Advent of Code\2015\Task 17.txt").readlines():
    data = line.split()
    towns.append(data[0])
    towns.append(data[2])
    dist_array[f'{data[0]}|{data[2]}'] = int(data[4])

towns = set(towns)
perms = list(itertools.permutations(towns, len(towns)))

def calc_tot_distance(perm, tot_dist = 0):
    for i in range(len(perm)-1):
        try:
            distance = dist_array[f'{perm[i]}|{perm[i+1]}']
        except KeyError:
            distance = dist_array[f'{perm[i+1]}|{perm[i]}']
        tot_dist += distance
    return tot_dist

current_distance = np.Inf

for perm in perms:
    new_distance = calc_tot_distance(perm)
    if new_distance < current_distance:
        current_distance = new_distance
        route = perm
    
print(f'Answer 17: {current_distance}')

current_distance = 0

for perm in perms:
    new_distance = calc_tot_distance(perm)
    if new_distance > current_distance:
        current_distance = new_distance
        route = perm
    
print(f'Answer 18: {current_distance}')

# %% TASK 19 & 20


data = "3113322113"

def count_matching_consec_digits(string):
    

def look_and_say(string, new_string = ""):
    for i in range(0, len(string), 2):
        new_chunk = int(string[i]) * string[i]
        new_string += new_chunk
    return new_string

data = look_and_say(data)
