import math
import numpy as np
import matplotlib.pyplot as plt
from string import ascii_letters as asc

# %% DAY 1

cal_total = 0
total_counter = []

for line in open(r"day 1.txt").readlines():
    if line == "\n":
        total_counter.append(cal_total)
        cal_total = 0
    else:
        cal_total += int(line)
        
print(f'Answer 1: {(max(total_counter))}')

total_counter.sort(reverse=True)

print(f'Answer 2: {sum(total_counter[:3])}')


# %% DAY 2

score = 0

for line in open(r"day 2.txt").readlines():
    if line[0] == "A":
        match line[2]:
            case "X":
                score += 4
            case "Y":
                score += 8
            case"Z":
                score += 3
    if line[0] == "B":
        match line[2]:
            case "X":
                score += 1
            case "Y":
                score += 5
            case"Z":
                score += 9
    if line[0] == "C":
        match line[2]:
            case "X":
                score += 7
            case "Y":
                score += 2
            case"Z":
                score += 6
print(f'Answer 3: {score}')

score = 0

for line in open(r"day 2.txt").readlines():
    if line[0] == "A":
        match line[2]:
            case "X":
                score += 3
            case "Y":
                score += 4
            case"Z":
                score += 8
    if line[0] == "B":
        match line[2]:
            case "X":
                score += 1
            case "Y":
                score += 5
            case"Z":
                score += 9
    if line[0] == "C":
        match line[2]:
            case "X":
                score += 2
            case "Y":
                score += 6
            case"Z":
                score += 7
print(f'Answer 4: {score}')


# %% DAY 3

priority_scores = {char:i+1 for i,char in enumerate(asc)}
priority_score = 0

for line in open(r"day 3.txt").readlines():
    line = line.replace("\n", "")
    num_items = len(line)
    comp_size = int(num_items/2)
    comp_1 = line[:comp_size]
    comp_2 = line[comp_size:]
    common = list(set(comp_1) & set(comp_2))[0]
    priority = priority_scores[common]
    priority_score += priority
    
print(f'Answer 5: {priority_score}')

lines = []
priority_score = 0

for line in open(r"day 3.txt").readlines():
    line = line.replace("\n", "")
    lines.append(line)

for i in range(len(lines)//3):
        common = list(set(lines[3*i]) & set(lines[3*i+1]) & set(lines[3*i+2]))[0]
        priority = priority_scores[common]
        priority_score += priority

print(f'Answer 6: {priority_score}')

# %% DAY 4

sections = [i for i in range(100)]
count = 0

for line in open(r"Day 4.txt").readlines():
    elf_1, elf_2 = line.replace("\n", "").split(",")
    e1_1, e1_2 = elf_1.split("-")
    e2_1, e2_2 = elf_2.split("-")
    
    if (all(x in sections[int(e1_1):int(e1_2)+1] for x in sections[int(e2_1):int(e2_2)+1])):
        count += 1
    elif (all(x in sections[int(e2_1):int(e2_2)+1] for x in sections[int(e1_1):int(e1_2)+1])):
        count += 1
    else:
        pass
    
print(f'Answer 7: {count}')

sections = [i for i in range(100)]
count = 0

for line in open(r"day 4.txt").readlines():
    elf_1, elf_2 = line.replace("\n", "").split(",")
    e1_1, e1_2 = elf_1.split("-")
    e2_1, e2_2 = elf_2.split("-")
    
    if (any(x in sections[int(e1_1):int(e1_2)+1] for x in sections[int(e2_1):int(e2_2)+1])):
        count += 1
    elif (any(x in sections[int(e2_1):int(e2_2)+1] for x in sections[int(e1_1):int(e1_2)+1])):
        count += 1
    else:
        pass
    
print(f'Answer 8: {count}')
    
# %% DAY 5

#Grab from work

# %% DAY 6
string = open(r"Day 6.txt").read()


def find_non_repeats(data, num_distinct):
    for i, char in enumerate(data):
        window = data[i: i+num_distinct]
        if len(set(window)) == num_distinct:
            return i+num_distinct

print(f'Answer 11: {find_non_repeats(string, 4)}')
print(f'Answer 12: {find_non_repeats(string, 14)}')


# %% DAY 7

def change_directory(folder):
    pass

def sum_contents(folder):
    pass

root = {}

for line in open(r'day 7.txt').readlines():
    command_line = line.split()
    if command_line[0] == "\$":
        if command_line[1] == "cd":
            root[command_line[2]] == {}
        else:
            pass
    elif command_line[0] == "dir":
        pass
    else:
        pass

# %% DAY 8
arr = []

for line in open(r'day 8.txt').readlines():
    string = line.rstrip("\n")
    arr.append([*string])

forest = np.array(arr).astype('int64')

def visible_trees(tree_line):
    visible_arr = []
    for i, tree in enumerate(tree_line):
        if all(tree > prev_tree for prev_tree in tree_line[:i]):
            visible_arr.append(1)
        else:
            visible_arr.append(0)
    return visible_arr

def visible_forest(forest):
    visible_forest = []
    for row in forest:
        visible_forest.append(visible_trees(row))    
    return np.array(visible_forest)

left = visible_forest(forest)

top_view = np.rot90(forest, 1)
right_view = np.rot90(forest, 2)
bottom_view = np.rot90(forest, 3)

top = visible_forest(top_view)
top = np.rot90(top, 3)

right = visible_forest(right_view)
right = np.rot90(right, 2)

bottom = visible_forest(bottom_view)
bottom = np.rot90(bottom, 1)

left_top = np.bitwise_or(left, top)
right_bottom = np.bitwise_or(right, bottom)
full = np.bitwise_or(left_top, right_bottom)

plt.imshow(full, cmap='hot', interpolation='nearest')
print(f'Answer 15: {full.sum()}')



def row_visibility(tree_line):
    scores = []
    for i, treehouse in enumerate(tree_line):
        if i == (len(tree_line)-1):
            scores.append(0)
        else:
            for j, tree in enumerate(tree_line[i:]):
                if j == 0:
                    continue
                if tree >= treehouse:
                    scores.append(j)
                    break
            else:
                scores.append(len(tree_line) - (i+1))
    return scores

def visibility_array(forest):
    visible_forest = []
    for row in forest:
        visible_forest.append(row_visibility(row))    
    return np.array(visible_forest)


right = visibility_array(forest)

top_view = np.rot90(forest, 1)
right_view = np.rot90(forest, 2)
bottom_view = np.rot90(forest, 3)

down = visibility_array(top_view)
down = np.rot90(down, 3)

left = visibility_array(right_view)
left = np.rot90(left, 2)

up = visibility_array(bottom_view)
up = np.rot90(up, 1)

left_top = np.multiply(left, down)
right_bottom = np.multiply(right, up)
full = np.multiply(left_top, right_bottom)

        
plt.imshow(full, cmap='hot', interpolation='nearest')
print(f'Answer 16: {full.max()}')      
            
# %% DAY 9

 
def up(start_pos):
    return (start_pos[0], start_pos[1]-1)

def down(start_pos):
    return (start_pos[0], start_pos[1]+1)

def left(start_pos):
    return (start_pos[0]-1, start_pos[1])

def right(start_pos):
    return (start_pos[0]+1, start_pos[1])
    
def h_t_touch(h_pos, t_pos):
    if (abs(h_pos[0] - t_pos[0]) <= 1) and (abs(h_pos[1] - t_pos[1]) <= 1):
        return True
    else:
        return False
    

def move_head(direction, h_pos):
    match direction:
        case "U":
            h_pos = up(h_pos)
        case "D":
            h_pos = down(h_pos)
        case "L":
            h_pos = left(h_pos)
        case "R":
            h_pos = right(h_pos)
#    print(f'head at pos: {h_pos}')
    return h_pos
    
    
def move_tail(h_pos, t_pos):
    displacement = (t_pos[0] - h_pos[0], t_pos[1] - h_pos[1])
    if displacement[0] <= -2:
        return(h_pos[0]-1, h_pos[1])
    elif displacement[0] >= 2:
        return(h_pos[0]+1, h_pos[1])
    elif displacement[1] <= -2:
        return(h_pos[0], h_pos[1]-1)
    elif displacement[1] >= 2:
        return(h_pos[0], h_pos[1]+1)


# Task 17
bridge = np.zeros((600, 600))

H = (300, 300)
T = (300, 300)

bridge[T] = 1

for instruction in open(r'Day 9.txt'):
    direction, distance =  instruction.split()
    distance = int(distance)
    for _ in range(distance):
        H = move_head(direction, H)                
        if h_t_touch(H, T):
            continue
        else:
            T = move_tail(H, T)            
        bridge[T] += 1
    
plt.imshow(bridge, cmap='hot', interpolation='nearest')
print(f'Answer 17: {(bridge > 0).sum()}')


# Task 18
bridge = np.zeros((450, 450))

bridge[(300, 300)] = 1
rope = [(300, 300)] * 10

for instruction in open(r'Day 9.txt'):
    direction, distance =  instruction.split()
    distance = int(distance)
    
    for _ in range(distance):
        rope[0] = move_head(direction, rope[0])  
        for i, knot in enumerate(rope):
            if i == len(rope)-1:
                bridge[rope[-1]] += 1
                break
            if h_t_touch(rope[i], rope[i+1]):
                continue
            else:
                rope[i+1] = move_tail(rope[i], rope[i+1])
#    print(f'after moving head {direction} by {distance} moves, rope postitions are: {rope}')
    
plt.imshow(bridge, cmap='hot', interpolation='nearest')
print(f'Answer 18: {(bridge > 0).sum()}')

# %% DAY 10

def create_CRT(signals, n=40):
    for i in range(0, len(signals), n):
        yield signals[i:i + n]

CPU = [1]

for line in open(r'Day 10.txt'):
    instruction = line.rstrip("\n").split()
    if instruction[0] == 'noop':
        CPU.append(CPU[-1])
    elif instruction[0] == 'addx':
        CPU.append(CPU[-1])
        new_x = CPU[-1] + int(instruction[1])
        CPU.append(new_x)
        
important_signals = [20, 60, 100, 140, 180, 220]

tot_signal_strength = sum([CPU[i] * i for i in important_signals])

print(f'Answer 19: {tot_signal_strength}')

CRT = list(create_CRT(CPU[1:]))
image = []


for row in CRT:
    image_line = []
    for cycle, signal in enumerate(row,1):
        if cycle in [signal-1, signal, signal+1]:
            image_line.append("X")
        else:
            image_line.append(".")
    image.append(image_line)

for i in range(len(image)):
    print(''.join(image[i]))
    
# %% DAY 11

class monkey():
    def __init__(self, items, operand, op_val, test, true, false):
        self.items = items
        self.op = operand
        self.op_val = op_val
        self.test = test
        self.true = true
        self.false = false

    def operation(self, item):
        if self.op == '+':
            return monkey.add(self, item)
        elif self.op == '*':
            return monkey.multiply(self, item)
        
    def add(self, item):
        if self.op_val == 'old':
            return item + item
        else:
            return item + int(self.op_val)
  
    def multiply(self, item):
        if self.op_val == 'old':
            return item * item

        else:
            return item * int(self.op_val)
       
    def div_test(self, item):
        return item % self.test == 0



with open(r'Day 11.txt') as f:
    data = [line.strip() for line in f]
    

    
def create_monkeys(num_monkeys, monkeys = {}):
    for i in range(num_monkeys):
        items = [int(item.strip(",")) for item in data[7*i + 1].split()[2:]]
        operand = data[7*i + 2].split()[4]
        op_val = data[7*i + 2].split()[5]
        test = int(data[7*i + 3].split()[3])
        true = int(data[7*i + 4].split()[5])
        false = int(data[7*i + 5].split()[5])
        monkeys[i] = monkey(items, operand, op_val, test, true, false)
    return monkeys

def play_keep_away_v1(rounds, monkeys):
    count = [0]*len(monkeys)
    for rnd in range(rounds):
        for m in monkeys:
            for item in monkeys[m].items:
                count[m] += 1
                new = monkeys[m].operation(item)
                reduced_new = new % lcm
                trunc_new = math.trunc(reduced_new/3)
                test_res = monkeys[m].div_test(trunc_new)
                if test_res:
                    monkeys[monkeys[m].true].items.append(trunc_new)
                else:
                    monkeys[monkeys[m].false].items.append(trunc_new)
            monkeys[m].items = []
    return count
            
def play_keep_away_v2(rounds, monkeys):
    count = [0]*len(monkeys)
    for rnd in range(rounds):
        for m in monkeys:
            for item in monkeys[m].items:
                count[m] += 1
                new = monkeys[m].operation(item)
                reduced_new = new % lcm
                test_res = monkeys[m].div_test(reduced_new)
                if test_res:
                    monkeys[monkeys[m].true].items.append(reduced_new)
                else:
                    monkeys[monkeys[m].false].items.append(reduced_new)
            monkeys[m].items = []
    return count

lcm = 9699690 # np.lcm.reduce([monkeys[m].test for m in monkeys])

monkeys = create_monkeys(8)
count = play_keep_away_v1(20, monkeys)

count.sort()        
print(f'Answer 21: {count[-2] * count[-1]}')


monkeys = create_monkeys(8)
count = play_keep_away_v2(10000, monkeys)

count.sort()        
print(f'Answer 22: {count[-2] * count[-1]}')

# %% DAY 12

def up(y,x):
    if y == 0:
        return np.nan
    else:
        return grid[y-1, x]

def right(y,x):
    if x == 82:
        return np.nan
    else:
        return grid[y, x+1]

def down(y,x):
    if y == 40:
        return np.nan
    else:
        return grid[y+1, x]

def left(y,x):
    if x == 0:
        return np.nan
    else:
        return grid[y, x-1]

def valid_coord_climb(current_pos):
    (y,x) = current_pos
    valid_moves = []
    if up(y,x) <= grid[current_pos] + 1:
        valid_moves.append((y-1, x))
        
    if right(y,x) <= grid[current_pos] + 1:
        valid_moves.append((y, x+1))
        
    if down(y,x) <= grid[current_pos] + 1:
        valid_moves.append((y+1, x))
        
    if left(y,x) <= grid[current_pos] + 1:
        valid_moves.append((y, x-1))
    return valid_moves

def valid_coord_drop(current_pos):
    (y,x) = current_pos
    valid_moves = []
    if up(y,x) >= grid[current_pos] - 1:
        valid_moves.append((y-1, x))
        
    if right(y,x) >= grid[current_pos] - 1:
        valid_moves.append((y, x+1))
        
    if down(y,x) >= grid[current_pos] - 1:
        valid_moves.append((y+1, x))
        
    if left(y,x) >= grid[current_pos] - 1:
        valid_moves.append((y, x-1))
    return valid_moves   

def climb(start, step=1, save_coords=[]):
    BFS = np.zeros(grid.shape)
    BFS = BFS - 1
    BFS[start] = 0
    val_coords = make_step_climb(BFS, start, step)
    while BFS[end] == -1:
        step += 1
        for c in val_coords:
            temp_val_coords = make_step_climb(BFS, c, step)
            save_coords.append(temp_val_coords)
        val_coords = set([coord for sublist in save_coords for coord in sublist])
        save_coords = []
        plt.imshow(BFS, cmap='hot', interpolation='nearest')
    return BFS, step

def drop(start, step=1, save_coords=[]):
    BFS = np.zeros(grid.shape)
    BFS = BFS - 1
    BFS[end] = 0
    val_coords = make_step_drop(BFS, end, step)
    while True:
        step += 1
        for c in val_coords:
            temp_val_coords = make_step_drop(BFS, c, step)
            save_coords.append(temp_val_coords)
            if grid[c] == 1:
                return BFS, step
        val_coords = set([coord for sublist in save_coords for coord in sublist])
        save_coords = []
        plt.imshow(BFS, cmap='hot', interpolation='nearest')

def make_step_climb(BFS, current_pos, step):
    valid_coords = valid_coord_climb(current_pos)
    for coord in valid_coords:
        if BFS[coord] == -1:
            BFS[coord] = step
    return valid_coords

def make_step_drop(BFS, current_pos, step):
    valid_coords = valid_coord_drop(current_pos)
    for coord in valid_coords:
        if BFS[coord] == -1:
            BFS[coord] = step
    return valid_coords


lines = []
elevation = {char:i for i,char in enumerate(asc[:26],1)}
elevation['S'] = 0
elevation['E'] = 27

for line in open(r'Day 12.txt').readlines():
    line = list(line.strip("\n"))
    new_line = [elevation[letter] for letter in line]
    lines.append(new_line)

    
grid = np.array(lines)
plt.imshow(grid, cmap='hot', interpolation='nearest')


start = tuple(np.argwhere(grid == 0)[0])
end = tuple(np.argwhere(grid == 27)[0])
grid[start] = 1
grid[end] = 26

BFS, step = climb(start)      
print(f'Answer 23: {step}')

BFS, step = drop(end)      
print(f'Answer 24: {step-1}')      

# %% DAY 13
